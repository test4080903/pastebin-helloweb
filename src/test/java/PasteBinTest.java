import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class PasteBinTest {
    @Test
    public void testCreateNewPaste() {
        WebDriver driver = new ChromeDriver();
        driver.get("https://pastebin.com/");
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));

        //Code text area
        WebElement textArea = wait.until(ExpectedConditions.presenceOfElementLocated(
                        By.xpath("//textarea[@id=\"postform-text\"]")));
        textArea.sendKeys(new String[]{"Hello from WebDriver"});

        //Expiration dropdown
        WebElement expirationDropdown = driver.findElement(
                By.xpath("//span[@id='select2-postform-expiration-container']"));
        expirationDropdown.click();
        WebElement expirationOption = driver.findElement(
                By.xpath("//ul[@role=\"listbox\"]//li[text()=\"10 Minutes\"]"));
        expirationOption.click();

        /*JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500)", "");
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement element =  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id=\"postform-expiration\"]")));
        Select expirationSelect = new Select(element);
        expirationSelect.selectByValue("10M");*/


        // Paste Name / Title:
        WebElement title = driver.findElement(
                By.xpath("//input[@id=\"postform-name\"]"));
        title.sendKeys(new String[]{"helloweb"});

        // Create New Paste button
        WebElement createButton = driver.findElement(
                By.xpath("//button[text()=\"Create New Paste\"]"));
        createButton.click();

        wait.until(ExpectedConditions.titleContains("helloweb"));
        System.out.println("Title: "+ driver.getTitle());
        driver.quit();
    }
}
